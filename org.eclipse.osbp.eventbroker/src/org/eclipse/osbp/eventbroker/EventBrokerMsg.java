/**
 *                                                                            
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *                                                                            
 * All rights reserved. This program and the accompanying materials           
 * are made available under the terms of the Eclipse Public License 2.0        
 * which accompanies this distribution, and is available at                  
 * https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 * SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 * Contributors:   
 * Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation 
 */
package org.eclipse.osbp.eventbroker;

import org.eclipse.osbp.ui.api.user.IUser;

public class EventBrokerMsg {
	public static final String ORGCHART_SELECT = "orgChartSelect"; //$NON-NLS-1$
	public static final String BPMNCHART_EVENT = "org/eclipse/osbp/vaaclipse/bpmn/demo/views/bpmnChartEvent"; //$NON-NLS-1$

	//**************** Dialog DSL ***********************
	public static final String DIALOG_STRATEGY_CHANGED = "org/eclipse/osbp/xtext/dialogdsl/strategychanged"; //$NON-NLS-1$

	//**************** Action DSL ***********************
//	public static final String ACTION_BUTTON_EXECUTE_PREFIX = "org/eclipse/osbp/xtext/actiondsl/execute/"; //$NON-NLS-1$
//	//**************** Menu DSL ***********************
//	public static final String MENU_CLOSE = "menu/close"; //$NON-NLS-1$
//	//**************** Menu DSL ***********************
//	public static final String QUERY_ANSWER = "query/answer"; //$NON-NLS-1$
//	public static final String QUERY_YES = "query/yes"; //$NON-NLS-1$
//	public static final String QUERY_NO = "query/no"; //$NON-NLS-1$
//	public static final String QUERY_OK = "query/ok"; //$NON-NLS-1$

	private String id;
	private String name;
	private long longVal;
	private int intVal;
	private Object object1;
	private Object object2;
	private IUser user;
	private IUser alternateUser;

	public EventBrokerMsg() {
		this(null, null, -1);
	}

	public EventBrokerMsg(String id) {
		this(id, null, -1);
	}

	public EventBrokerMsg(String id, String name) {
		this(id, name, -1);
	}

	public EventBrokerMsg(String id, String name, long longVal) {
		this(id, name, longVal, -1);
	}

	public EventBrokerMsg(String id, String name, long longVal, int intVal) {
		super();
		this.id = id;
		this.name = name;
		this.longVal = longVal;
		this.intVal = intVal;
	}
	
	public EventBrokerMsg(int id, String name) {
		this(id, name, -1);
	}

	public EventBrokerMsg(int id, String name, long longVal) {
		this(id, name, longVal, -1);
	}

	/**
	 * Using the id {@link EventBrokerMsg.REFRESH_VIEW} the @param intVal has the value 0 in case of a previously realized update but 1 in case of an insert.
	 *
	 * @param id the id
	 * @param name the name
	 * @param longVal the long val
	 * @param intVal the int val
	 */
	public EventBrokerMsg(int id, String name, long longVal, int intVal) {
		super();
		this.id = new Integer(id).toString();
		this.name = name;
		this.longVal = longVal;
		this.intVal = intVal;
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public long getLong() {
		return longVal;
	}

	public void setLong(long longVal) {
		this.longVal = longVal;
	}

	public int getInt() {
		return intVal;
	}

	public void setInt(int intVal) {
		this.intVal = intVal;
	}

	public Object getObject1() {
		return object1;
	}

	public void setObject1(Object object1) {
		this.object1 = object1;
	}

	public Object getObject2() {
		return object2;
	}

	public void setObject2(Object object2) {
		this.object2 = object2;
	}

	public IUser getUser() {
		return user;
	}

	public void setUser(IUser user) {
		this.user = user;
	}

	public IUser getAlternateUser() {
		return alternateUser;
	}

	public void setAlternateUser(IUser alternateUser) {
		this.alternateUser = alternateUser;
	}

}
