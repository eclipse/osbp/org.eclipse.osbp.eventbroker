/*
 *                                                                            
 *  Copyright (c) 2011 - 2017 - Loetz GmbH & Co KG, 69115 Heidelberg, Germany 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Initial contribution:                                                      
 *     Loetz GmbH & Co. KG
 * 
 */
package org.eclipse.osbp.eventbroker;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import org.eclipse.osbp.runtime.common.event.EventDispatcherEvent;
import org.eclipse.osbp.runtime.common.event.IEventDispatcher;
import org.osgi.service.component.annotations.Component;

/**
 * The EventDispatcher service.
 */
@Component(service = IEventDispatcher.class)
public class EventDispatcher implements IEventDispatcher {

	/** The receiver list. */
	private List<WeakReference<Receiver>> receiverList = new CopyOnWriteArrayList<>();

	@Override
	public void addEventReceiver(Receiver receiver) {
		receiverList.add(new WeakReference<>(receiver));
	}

	@Override
	public void removeEventReceiver(Receiver receiver) {
		for (WeakReference<Receiver> ref : new ArrayList<>(receiverList)) {
			if (ref.get() == null || ref.get() == receiver) {
				receiverList.remove(ref);
			}
		}
	}

	@Override
	public void sendEvent(EventDispatcherEvent event) {
		for (WeakReference<Receiver> ref : new ArrayList<>(receiverList)) {
			if (ref.get() == null) {
				receiverList.remove(ref);
			} else {
				ref.get().receiveEvent(event);
			}
		}
	}
}
